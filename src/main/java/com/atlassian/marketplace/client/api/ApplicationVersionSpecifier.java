package com.atlassian.marketplace.client.api;

import io.atlassian.fugue.Either;

import java.util.Optional;

import static com.atlassian.marketplace.client.util.Convert.iterableOf;

/**
 * Encapsulates parameters that can be passed to {@link Applications#safeGetVersion(ApplicationKey, ApplicationVersionSpecifier)}.
 */
public final class ApplicationVersionSpecifier
{
    private final Optional<Either<String, Integer>> nameOrBuild;
    
    private ApplicationVersionSpecifier(Optional<Either<String, Integer>> nameOrBuild)
    {
        this.nameOrBuild = nameOrBuild;
    }
    
    /**
     * Searches for an application version by build number.
     */
    public static ApplicationVersionSpecifier buildNumber(int buildNumber)
    {
        return new ApplicationVersionSpecifier(Optional.of(Either.right(buildNumber)));
    }
    
    /**
     * Searches for an application version by name (version string, e.g. "1.0.0").
     */
    public static ApplicationVersionSpecifier versionName(String name)
    {
        return new ApplicationVersionSpecifier(Optional.of(Either.left(name)));
    }
    
    /**
     * Searches for the latest version.
     */
    public static ApplicationVersionSpecifier latest()
    {
        return new ApplicationVersionSpecifier(Optional.empty());
    }
    
    /**
     * Used internally to access the previously set details of the specifier.  
     */
    public Optional<Either<String, Integer>> safeGetSpecifiedVersion()
    {
        return nameOrBuild;
    }

    @Override
    public String toString()
    {
        for (Either<String, Integer> vob: iterableOf(nameOrBuild))
        {
            for (Integer b: vob.right())
            {
                return "buildNumber(" + b + ")";
            }
            for (String n: vob.left())
            {
                return "name(" + n + ")";
            }
        }
        return "latest";
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other instanceof ApplicationVersionSpecifier) && ((ApplicationVersionSpecifier) other).nameOrBuild.equals(this.nameOrBuild);
    }
    
    @Override
    public int hashCode()
    {
        return nameOrBuild.hashCode();
    }
}
