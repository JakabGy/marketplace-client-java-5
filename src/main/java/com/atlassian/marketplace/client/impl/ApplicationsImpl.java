package com.atlassian.marketplace.client.impl;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.ApplicationKey;
import com.atlassian.marketplace.client.api.ApplicationVersionSpecifier;
import com.atlassian.marketplace.client.api.ApplicationVersionsQuery;
import com.atlassian.marketplace.client.api.Applications;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.api.ProductQuery;
import com.atlassian.marketplace.client.api.QueryBounds;
import com.atlassian.marketplace.client.api.UriTemplate;
import com.atlassian.marketplace.client.model.Application;
import com.atlassian.marketplace.client.model.ApplicationVersion;
import com.atlassian.marketplace.client.util.UriBuilder;
import com.google.common.collect.ImmutableMap;
import io.atlassian.fugue.Either;

import java.net.URI;
import java.util.Optional;

import static com.atlassian.marketplace.client.impl.ApiHelper.getTemplatedLink;
import static com.atlassian.marketplace.client.util.Convert.iterableOf;

final class ApplicationsImpl extends ApiImplBase implements Applications
{
    ApplicationsImpl(ApiHelper apiHelper, InternalModel.MinimalLinks root) throws MpacException
    {
        super(apiHelper, root, "applications");
    }

    @Override
    public Optional<Application> safeGetByKey(ApplicationKey applicationKey) throws MpacException
    {
        InternalModel.Applications collectionRep = getEmptyBaseCollectionRep();
        UriTemplate byKeyTemplate = ApiHelper.requireLinkUriTemplate(collectionRep._links, "byKey", InternalModel.Applications.class);
        UriBuilder uri = UriBuilder.fromUri(byKeyTemplate.resolve(ImmutableMap.of("applicationKey", applicationKey.getKey())));
        return apiHelper.safeGetOptionalEntity(uri.build(), Application.class);
    }

    @Override
    public Optional<ApplicationVersion> safeGetVersion(ApplicationKey applicationKey, ApplicationVersionSpecifier versionQuery) throws MpacException
    {
        for (Application a: iterableOf(safeGetByKey(applicationKey)))
        {
            URI uri = getVersionUri(a, versionQuery);
            return apiHelper.safeGetOptionalEntity(uri, ApplicationVersion.class);
        }
        return Optional.empty();
    }

    @Override
    public Page<ApplicationVersion> getVersions(ApplicationKey applicationKey, ApplicationVersionsQuery versionsQuery) throws MpacException
    {
        for (Application a: iterableOf(safeGetByKey(applicationKey)))
        {
            UriTemplate ut = getVersionsUriTemplate(a);
            ImmutableMap.Builder<String, String> params = ImmutableMap.builder();
            for (int b: iterableOf(versionsQuery.safeGetAfterBuildNumber()))
            {
                params.put("afterBuildNumber", String.valueOf(b));
            }
            UriBuilder ub = UriBuilder.fromUri(ut.resolve(params.build()));
            ApiHelper.addHostingParam(versionsQuery, ub);
            ApiHelper.addBoundsParams(versionsQuery, ub);
            return apiHelper.getMore(new PageReference<ApplicationVersion>(
                ub.build(), versionsQuery.getBounds(), pageReader(InternalModel.ApplicationVersions.class)));
        }
        return Page.empty();
    }
   
    @Override
    public ApplicationVersion createVersion(ApplicationKey applicationKey, ApplicationVersion version) throws MpacException
    {
        for (Application a: iterableOf(safeGetByKey(applicationKey)))
        {
            return genericCreate(getVersionsUriTemplate(a).resolve(ImmutableMap.of()), version, Optional.empty());
        }
        throw new MpacException.ServerError(404);
    }

    @Override
    public ApplicationVersion updateVersion(ApplicationVersion original, ApplicationVersion updated) throws MpacException
    {
        return genericUpdate(original.getSelfUri(), original, updated);
    }
    
    private UriTemplate getVersionsUriTemplate(Application a) throws MpacException
    {
        return ApiHelper.requireLinkUriTemplate(a.getLinks(), "versions", Application.class);
    }
    
    private InternalModel.Applications getEmptyBaseCollectionRep() throws MpacException
    {
        UriBuilder uri = fromApiRoot();
        ApiHelper.addBoundsParams(ProductQuery.builder().bounds(QueryBounds.empty()).build(), uri);
        return apiHelper.getEntity(uri.build(), InternalModel.Applications.class);
    }

    private URI getVersionUri(Application a, ApplicationVersionSpecifier v) throws MpacException
    {
        for (Either<String, Integer> specifiedVersion: iterableOf(v.safeGetSpecifiedVersion()))
        {
            for (String name: specifiedVersion.left())
            {
                return getTemplatedLink(a, "versionByName", "name", name);
            }
            for (Integer build: specifiedVersion.right())
            {
                return getTemplatedLink(a, "versionByBuild", "applicationBuildNumber", String.valueOf(build));
            }
        }
        return apiHelper.requireLinkUri(a.getLinks(), "latestVersion", Application.class);
    }
}
