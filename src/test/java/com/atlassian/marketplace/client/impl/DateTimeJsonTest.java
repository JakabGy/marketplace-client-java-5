package com.atlassian.marketplace.client.impl;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class DateTimeJsonTest extends BaseJsonTests
{
    @Test
    public void dateTimeIsDecoded() throws Exception
    {
        assertThat(decode("{\"x\":\"2014-05-24T18:00:00.000Z\"}", HasDateTime.class).x,
            equalTo(new DateTime(2014, 05, 24, 18, 00, 00, 000, DateTimeZone.UTC)));
    }

    @Test
    public void localDateIsDecoded() throws Exception
    {
        assertThat(decode("{\"x\":\"2014-05-24\"}", HasLocalDate.class).x,
            equalTo(new LocalDate(2014, 05, 24)));
    }
    
    @Test
    public void localDateIsEncoded() throws Exception
    {
        HasLocalDate o = new HasLocalDate();
        o.x = new LocalDate(2014, 05, 24);
        assertThat(encode(o), equalTo("{\"x\":\"2014-05-24\"}"));
    }
    
    static final class HasDateTime
    {
        DateTime x;
    }
    
    static final class HasLocalDate
    {
        LocalDate x;
    }
}
